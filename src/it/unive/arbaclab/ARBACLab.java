package it.unive.arbaclab;

import it.unive.arbaclab.algorithm.RoleReachabilityAlgorithm;
import it.unive.arbaclab.models.Policy;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;

public class ARBACLab {
	
	public static void main(String args[]) throws Exception {
		ArgumentParser arguments = ArgumentParsers.newFor("ARBACLab").build()
				.defaultHelp(true)
                .description("ARBACLab - A simple Role Reachability Problem solver");
		
		// Policy to validate
		arguments.addArgument("-p", "--policy")
				.required(true)
                .help("File containing the policy to validate");
		
		Namespace ns = arguments.parseArgs(args);	
		
		// Starting a new PolicyParser
		PolicyParser parser = new PolicyParser();
		Policy policy = parser.parse(ns.getString("policy"));
		
		if (policy != null) {
			boolean valid = policy.isValid();
			
			System.out.println("Loaded " + (!valid ? "in" : "")+ "valid Policy\n\n" + policy.toString());
			
			if (valid) {
				System.out.println("\nStarting Role Reachability Algorithm for Goal: " + policy.getGoal());
				
				// Preparing a new Role Reachability Algorithm instance with the policy received as input
				RoleReachabilityAlgorithm algorithm = new RoleReachabilityAlgorithm(policy);
				
				// If true --> the goal has been reached
				if (algorithm.run()) {
					System.out.println("\nThe Goal has been reached");
				} else {
					System.out.println("\nThe Goal has not been reached");
				}
			}
		}
	}
}
