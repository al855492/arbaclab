package it.unive.arbaclab.algorithm;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import it.unive.arbaclab.models.Policy;
import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.User;
import it.unive.arbaclab.models.UserToRole;
import it.unive.arbaclab.models.rules.CanAssignRule;
import it.unive.arbaclab.models.rules.CanRevokeRule;
import it.unive.arbaclab.models.rules.CanRule;
import it.unive.arbaclab.models.rules.RuleCondition;

public class RoleReachabilityAlgorithm {
	private Policy policy;
	private List<RuleAlgorithm> rules = new LinkedList<>();
	private List<StateAlgorithm> states = new LinkedList<>();
	private Set<String> historyStates = new TreeSet<>();
	
	public RoleReachabilityAlgorithm(Policy policy) {
		this.policy = policy;
	}
	
	public boolean run() throws Exception {
		// Step 1: Loading the initial state
		this.states.add(this.loadInitialStateForAlgorithm());
		
		// Step 2: Preparing rules to be executed
		this.loadRulesForAlgorithm();
		
		// Step 3: Applying Backward Slicing
		this.applyBackwardSlicing();
		
		// Step 4: Preparing looping variables
		boolean modified = true;
		boolean goal = false;

		Set<StateAlgorithm> statesToAdd = new LinkedHashSet<>();
		Set<StateAlgorithm> statesToRemove = new LinkedHashSet<>();
		
		// We loop until we have reached the goal or there are new changes
		while (modified && !goal) {
			modified = false;
			
			// Iterate over all states
			for (StateAlgorithm state : this.states) {
				String stateID = state.identifier();
				
				// Mark this state as 'visited'
				this.historyStates.add(stateID);
				
				for (RuleAlgorithm rule : this.rules) {	
					// Try to apply all available rules to current state
					StateAlgorithm newState = rule.apply(state);
					
					// When a rule is applied, a new state is returned
					if (newState != null) {
						// Checking if the new state has been already navigated
						if (!this.historyStates.contains(newState.identifier())) {
							// If not, add the new state
							statesToAdd.add(newState);
							
							modified = true;
							
							// If the rule just applied targeted our goal then we conclude the search
							if (rule.goal(this.policy.getGoal())) {
								goal = true;
								break;
							}
		
						}				
					}						
				}

				// In the current state all the rules have been applied. 
				// We can mark it as to be removed
				statesToRemove.add(state);

				if (goal) break;
			}		
			
			// Adding all new states to be visited
			this.states.removeAll(statesToRemove);
			// Removing all states already visited
			this.states.addAll(statesToAdd);
			
			// Clear resources
			statesToRemove.clear();
			statesToAdd.clear();
		}
		
		return goal;
	}
	
	private StateAlgorithm loadInitialStateForAlgorithm() {
		StateAlgorithm state = new StateAlgorithm();
		
		// Convert list of users into map (for performance reasons)
		for (User user : policy.getUsers()) {
			state.getState().put(user.getName(), new UserAlgorithm(user));
		}
		
		// Assign initial roles to users
		for (UserToRole ur : policy.getUa()) {
			state.getState().get(ur.getUser().getName()).assignRole(ur.getRole());
		}
		
		return state;
	}
	
	private void loadRulesForAlgorithm() {
		// Loading assign-rules
		for (CanAssignRule ar : policy.getCa()) {
			this.rules.add(new AssignRuleAlgorithm(ar));
		}
		
		// Loading revoke-rules
		for (CanRevokeRule rr : policy.getCr()) {
			this.rules.add(new RevokeRuleAlgorithm(rr));
		}
	}
	
	private void applyBackwardSlicing() {
		Set<Role> state = new LinkedHashSet<Role>();
		
		// We start from the state S[0] = {Rtarget} 
		state.add(this.policy.getGoal());
		
		// Detecting fixed point
		boolean fixedpoint = false;
		
		// We iterate until we reach a fixed point
		while (!fixedpoint) {
			fixedpoint = true;
			
			for (RuleAlgorithm rule : this.rules) {
				// We are interested only in CanAssign rules
				if (rule instanceof AssignRuleAlgorithm) {
					CanAssignRule ar = (CanAssignRule) rule.getInnerRule();
					
					// If S[i-1] contains our target role
					if (state.contains(ar.getTarget())) {
						int initSize = state.size();
						
						// We add to S[i] our Ra (has role)
						state.add(ar.getHas());
						
						// Add all Rp (Positive Roles) and Rn (Negative Roles)
						for (RuleCondition cond : ar.getConditions()) {
							if (!cond.isAlwaysTrue()) {
								state.add(cond.getRole());
							}
						}
						
						// If the number of elements is changed, 
						// we can go on with the iterations
						if (state.size() != initSize) {
							fixedpoint = false;
						}
					}
				}
			}
		}
		
		// Let S* be the fixed point.
		// We calculate the set R \ S*
		Set <Role> rolesToRemove = new LinkedHashSet<Role>();
		
		for (Role role : this.policy.getRoles()) {
			if (!state.contains(role)) {
				rolesToRemove.add(role);
			}
		}
		
		// 1) We ignore all rules that assign a role in R \ S*
		// 2) We ignore all rules that revoke a role in R \ S*
		for (int i=this.rules.size()-1; i >= 0; i--) {
			CanRule innerRule = this.rules.get(i).getInnerRule();
			
			if (rolesToRemove.contains(innerRule.getTarget())) {
				this.rules.remove(i);
			}
		}
	}
}
