package it.unive.arbaclab.algorithm;

import java.util.Map;

import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.rules.CanRule;

public abstract class RuleAlgorithm {
	public RuleAlgorithm() {}

	public abstract boolean goal(Role goal);
	protected abstract CanRule getInnerRule();

	private boolean precondition(StateAlgorithm state) throws Exception {
		boolean precondition = false;
		
		CanRule rule = this.getInnerRule();
		
		if (rule != null) {
			// Looking for an user which verify the precondition
			for (Map.Entry<String, UserAlgorithm> entry : state.getState().entrySet()) {
				UserAlgorithm user = entry.getValue();
				
				// Check if the user contains the required role
				precondition = user.getRoles().contains(rule.getHas());

				if (precondition)
					break;
			}
		}

		return precondition;
	}
	
	public StateAlgorithm apply(StateAlgorithm state) throws Exception {
		StateAlgorithm newState = null;

		if (this.precondition(state)) {
			CanRule rule = this.getInnerRule();
			
			// Looking for the user in which apply the rule
			for (Map.Entry<String, UserAlgorithm> entry : state.getState().entrySet()) {
				UserAlgorithm user = entry.getValue();
				
				if (rule.satisfy(user.getUser(), user.getRoles())) {
					if (newState == null)
						newState = (StateAlgorithm) state.clone();

					UserAlgorithm toModify = newState.getState().get(entry.getKey());
					rule.apply(toModify.getUser(), toModify.getRoles());
				}
			}
		}

		return newState;
	}
}
