package it.unive.arbaclab.algorithm;

import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.rules.CanRevokeRule;
import it.unive.arbaclab.models.rules.CanRule;

public class RevokeRuleAlgorithm extends RuleAlgorithm {
	private CanRevokeRule revokeRule;
	
	public RevokeRuleAlgorithm(CanRevokeRule rr) {
		this.revokeRule = rr;
	}

	@Override
	public boolean goal(Role goal) {
		return false;
	}

	@Override 
	protected CanRule getInnerRule() {
		return this.revokeRule;
	}
}
