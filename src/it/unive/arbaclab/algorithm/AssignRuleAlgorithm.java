package it.unive.arbaclab.algorithm;

import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.rules.CanAssignRule;
import it.unive.arbaclab.models.rules.CanRule;

public class AssignRuleAlgorithm extends RuleAlgorithm {
	private CanAssignRule assignRule;
	
	public AssignRuleAlgorithm(CanAssignRule ar) {
		this.assignRule = ar;
	}
	
	@Override
	public boolean goal(Role goal) {
		return (this.assignRule != null && this.assignRule.getTarget().equals(goal));
	}
	
	@Override 
	protected CanRule getInnerRule() {
		return this.assignRule;
	}
}
