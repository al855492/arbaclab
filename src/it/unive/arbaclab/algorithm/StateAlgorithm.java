package it.unive.arbaclab.algorithm;

import java.util.Map;
import java.util.TreeMap;

import it.unive.arbaclab.interfaces.Identifiable;

/*
 * A StateAlgorithm is a snapshot of user and related roles
 * in an instant. Each state has a unique identifier and 
 * the possibility to be cloned in order to generate a new state
 * that can be modified
 */
public class StateAlgorithm implements Cloneable, Identifiable {
	private Map<String, UserAlgorithm> state = new TreeMap<>();
	
	public StateAlgorithm() {}
	
	public Map<String, UserAlgorithm> getState(){
		return this.state;
	}
	
	/*
	 * Debug method for printing out the state in the following form
	 * {(U1,R1),(U1,R2),....(UN,RN)}
	 */
	public String printState() {
		StringBuilder status = new StringBuilder();
		
		status.append("{");
		
		boolean first = true;
		for (Map.Entry<String, UserAlgorithm> entry : this.state.entrySet()) {
			String temp = entry.getValue().toString();
			
			if (temp != null && !temp.trim().isEmpty()) {
				if (!first) {
					status.append(",");
				} else {
					first = !first;
				}
				
				status.append(entry.getValue().toString());	
			}			
		}
		
		status.append("}");
		
		return status.toString();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException{  
		StateAlgorithm sa = new StateAlgorithm();
		
		// Cloning by hands the users' data
		for (Map.Entry<String, UserAlgorithm> entry : this.state.entrySet()) {
			sa.state.put(entry.getKey(), (UserAlgorithm) entry.getValue().clone());
		}
		
		return sa;
	}
	
	@Override
	public String identifier() {
		StringBuilder identifier = new StringBuilder();
		
		// State identifier depends on useralgorithm's identifiers
		for (UserAlgorithm user : this.state.values()) {
			identifier.append(user.identifier());
		}
		
		return identifier.toString();
	}
}
