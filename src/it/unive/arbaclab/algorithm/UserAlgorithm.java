package it.unive.arbaclab.algorithm;

import java.util.Set;
import java.util.TreeSet;

import it.unive.arbaclab.interfaces.Identifiable;
import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.User;

/**
 * A UserAlgorithm is an instance of an User with it's related Roles.
 * Every instance has a unique identifier and can be cloned.
 */
public class UserAlgorithm implements Cloneable, Identifiable {
	private User user;
	/*
	 * Since the identifier is obtained by a for-loop we must keep the roles sorted.
	 * In this way we can't have same user with different role orders (and as conseguences id)
	 */
	private Set<Role> roles = new TreeSet<>();
	
	public UserAlgorithm(User user) {	
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public void assignRole(Role role) {
		this.roles.add(role);
	}
	
	public void revokeRole(Role role) {
		this.roles.remove(role);
	}
		
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		int i = 0;
		for (Role r : this.roles) {
			sb.append("(" + this.user.getName() + "," + r.getName() + ")");
			if (i<this.roles.size()-1)
				sb.append(", ");
			i++;
		}
		
		return sb.toString();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException{  
		UserAlgorithm ua = new UserAlgorithm(this.user);
		
		for (Role r : roles) {
			ua.roles.add(r);
		}
		
		return ua;
	}
	
	@Override
	public String identifier() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("U"+this.user.identifier());
		
		for (Role role : this.roles) {
			sb.append("R" + role.identifier());
		}
		
		return sb.toString();
	}
}
