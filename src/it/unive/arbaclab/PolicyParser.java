package it.unive.arbaclab;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

import it.unive.arbaclab.models.Policy;
import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.User;
import it.unive.arbaclab.models.UserToRole;
import it.unive.arbaclab.models.rules.CanAssignRule;
import it.unive.arbaclab.models.rules.CanRevokeRule;
import it.unive.arbaclab.models.rules.RuleCondition;
import it.unive.arbaclab.models.rules.RuleConditions;

public class PolicyParser {
	private static final String ROLES = "Roles";
	private static final String USERS = "Users";
	private static final String UA = "UA";
	private static final String CR = "CR";
	private static final String CA = "CA";
	private static final String GOAL = "Goal";
	
	public PolicyParser() {}
	
	public Policy parse(String fileName) {
		File file = new File(fileName);
		Policy policy = new Policy();
		
		try (Scanner sc = new Scanner(file, StandardCharsets.UTF_8.name())){
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				
				// Parsing the line based on its initial value
				if (line != null && !line.trim().isEmpty()) {
					if (line.startsWith(ROLES)) {
						policy.setRoles(parseRolesLine(line));
					} 
					else if (line.startsWith(USERS)) {
						policy.setUsers(parseUsersLine(line));
					} 
					else if (line.startsWith(UA)) {
						policy.setUa(parseUALine(line));
					} 
					else if (line.startsWith(CR)) {
						policy.setCr(parseCRLine(line));
					} 
					else if (line.startsWith(CA)){
						policy.setCa(parseCALine(line));
					} 
					else if (line.startsWith(GOAL)) {
						policy.setGoal(parseGoalLine(line));
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return policy;
	}
	
	private Set<Role> parseRolesLine(String line){
		Set <Role> set = new LinkedHashSet<>();
		
		// Preparing line for parsing
		line = cleanInputLine(line, ROLES);
		
		// Roles are separated by a space
		for (String role : line.split(" ")) {
			if (role != null && !role.trim().isEmpty()) {
				set.add(new Role(role.trim()));
			}
		}
		
		return set;	
	}
	
	private Set<User> parseUsersLine(String line){
		Set <User> set = new LinkedHashSet<>();
		
		// Preparing line for parsing
		line = cleanInputLine(line, USERS);
		
		// Users are separated by a space
		for (String user : line.split(" ")) {
			if (user != null && !user.trim().isEmpty()) {
				set.add(new User(user.trim()));
			}
		}
		
		return set;	
	}

	private Set<UserToRole> parseUALine(String line) {
		Set <UserToRole> set = new LinkedHashSet<>();
		
		// Preparing line for parsing
		line = cleanInputLine(line, UA);
		
		for (String pair : line.split(" ")) {
			// An entity consist of a User and a Role (<User,Role>)
			String [] entity = splitIntoEntities(pair);
			
			if (entity.length == 2) {
				set.add(new UserToRole(new User(entity[0].trim()), new Role(entity[1].trim())));
			}
		}		
		
		return set;
	}
	
	private Set<CanRevokeRule> parseCRLine(String line){
		Set <CanRevokeRule> set = new LinkedHashSet<>();
		
		// Preparing line for parsing
		line = cleanInputLine(line, CR);
		
		for (String pair : line.split(" ")) {
			//An entity consist of a pair of Roles (<Role,Role>)
			String [] entities = splitIntoEntities(pair);
			
			if (entities.length == 2) {
				set.add(new CanRevokeRule(new Role(entities[0].trim()), new Role(entities[1].trim())));	
			}
		}	
		
		return set;
	}
	
	private Set<CanAssignRule> parseCALine(String line){
		Set <CanAssignRule> set = new LinkedHashSet<>();
		
		// Preparing line for parsing
		line = cleanInputLine(line, CA);
		
		for (String pair : line.split(" ")) {
			// A CA entities consists of <Role,ListOfConditions,Role>
			String [] entities = splitIntoEntities(pair);
			
			if (entities.length == 3) {
				set.add(prepareAssignRuleFromEntities(entities));	
			}			
		}
		
		return set;
	}
	
	private CanAssignRule prepareAssignRuleFromEntities(String [] entities) {
		Role has = new Role(entities[0].trim());
		Role assign = new Role(entities[2].trim());
		
		RuleConditions conditions = new RuleConditions();
		
		// Several conditions are separated by the char &
		String [] conds = entities[1].split("&");
		
		for (String cond : conds) {
			// Handling always true policy
			if (Boolean.TRUE.toString().equalsIgnoreCase(cond)) {
				conditions.add(new RuleCondition(null, false, true));
			} else {
				boolean negative = false;
				
				// This is an inverse policy
				if (cond.startsWith("-")) {
					negative = true;
					cond = cond.substring(1);
				}
				
				conditions.add(new RuleCondition(new Role(cond.trim()), negative, false));
			}
		}
		
		return new CanAssignRule(has, conditions, assign);
	}
	
	private Role parseGoalLine(String line) {
		// Goal line contains only the role name to reach
		return new Role(cleanInputLine(line, GOAL));
	}
	
	private String cleanInputLine(String line, String entity) {
		// Removing the initial entity name, the final semicolon and double spacing
		return line.replace(entity + " ", "").replace(";", "").replace("  ", " ").trim();
	}
	
	private String [] splitIntoEntities(String pair) {
		return pair.replace("<", "").replace(">", "").split(",");
	}
}
