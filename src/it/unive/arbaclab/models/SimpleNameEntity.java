package it.unive.arbaclab.models;

import it.unive.arbaclab.interfaces.Identifiable;

public class SimpleNameEntity implements Identifiable {
	private String name;
	private Long identifier;
	
	private static Long IDENTIFIERS = 0l;
	
	public SimpleNameEntity(String name) {
		this.name = name;
		this.identifier = ++IDENTIFIERS;
	}
	
	public String getName() {
		return this.name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleNameEntity other = (SimpleNameEntity) obj;
		if (this.name == null) {
			if (other.getName() != null)
				return false;
		} else if (!this.name.equals(other.getName()))
			return false;
		return true;
	}
	
	@Override
	public String identifier() {
		return String.valueOf(this.identifier);
	}
	
	@Override
	public String toString() {
		return (this.getName() != null ? this.getName() : "");
	}
}
