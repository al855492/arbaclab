package it.unive.arbaclab.models;

import java.util.LinkedHashSet;
import java.util.Set;

import it.unive.arbaclab.interfaces.Validable;
import it.unive.arbaclab.models.rules.CanAssignRule;
import it.unive.arbaclab.models.rules.CanRevokeRule;
import it.unive.arbaclab.models.rules.RuleCondition;
import it.unive.arbaclab.models.rules.RuleConditions;

public class Policy implements Validable {
	private Set<Role> roles = new LinkedHashSet<>();
	private Set<User> users = new LinkedHashSet<>();
	private Set<UserToRole> ua = new LinkedHashSet<>();
	private Set<CanRevokeRule> cr = new LinkedHashSet<>();
	private Set<CanAssignRule> ca = new LinkedHashSet<>();
	
	private Role goal;
	
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	public Set<UserToRole> getUa() {
		return ua;
	}

	public void setUa(Set<UserToRole> ua) {
		this.ua = ua;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<CanRevokeRule> getCr() {
		return cr;
	}

	public void setCr(Set<CanRevokeRule> cr) {
		this.cr = cr;
	}
	
	public Set<CanAssignRule> getCa() {
		return ca;
	}

	public void setCa(Set<CanAssignRule> ca) {
		this.ca = ca;
	}

	public Role getGoal() {
		return goal;
	}

	public void setGoal(Role goal) {
		this.goal = goal;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Roles ");
		if (this.roles != null) {
			for (Role role : this.roles) {
				sb.append(role.getName() + " ");
			}
		}
		sb.append(";\n");
		
		sb.append("Users ");
		if (this.users != null) {
			for (User user : this.users) {
				sb.append(user.getName() + " ");
			}
		}
		sb.append(";\n");
		
		sb.append("UA ");
		if (this.ua != null) {
			for (UserToRole ur : this.ua) {
				sb.append(ur.toString() + " ");
			}	
		}
		sb.append(";\n");
		
		sb.append("CR ");
		if (this.cr != null) {
			for (CanRevokeRule rule : this.cr) {
				sb.append(rule.toString() + " ");
			}
		}
		sb.append(";\n");
		
		sb.append("CA ");
		if (this.ca != null) {
			for (CanAssignRule rule : this.ca) {
				sb.append(rule.toString() + " ");
			}
		}
		sb.append(";\n");
		
		sb.append("Goal ");
		if (this.goal != null) {
			sb.append(this.goal.getName() + " ");
		}
		sb.append(";");
		
		return sb.toString();
	}
	
	@Override
	public boolean isValid() {
		// Checking if exists at least one roles
		if (this.roles == null || this.roles.size() == 0) {
			return false;
		}
		
		// Checking if exists at least one users
		if (this.users == null || this.users.size() == 0) {
			return false;
		}
		
		if (this.ua == null) {
			return false;
		} else {
			// Checking if all UserToRole definitions refer to existing User and Role
			for (UserToRole ur : this.ua) {
				if (!this.users.contains(ur.getUser()) || !this.roles.contains(ur.getRole())) {
					return false;
				}
			}
		}
		
		if (this.cr == null) {
			return false;
		} else {
			// Checking if all RevokeRule refer to existing Roles
			for (CanRevokeRule rr : this.cr) {
				if (!this.roles.contains(rr.getHas()) || !this.roles.contains(rr.getTarget())) {
					return false;
				}
			}
		}
		
		if (this.ca == null) {
			return false;
		} else {
			// Checking if all AssignRule refers to existing Roles
			for (CanAssignRule ar : this.ca) {
				if (!this.roles.contains(ar.getHas()) || !this.roles.contains(ar.getTarget())) {
					return false;
				} else {
					RuleConditions conditions = ar.getConditions();
					
					for (RuleCondition cond : conditions) {
						// A condition is valid if its always TRUE or refers to a Role
						if ((cond.getRole() == null && !cond.isAlwaysTrue()) || (cond.getRole() != null && !this.roles.contains(cond.getRole()))) {
							return false;
						}
					}
				}
			}
		}
		
		return true;
	}
}
