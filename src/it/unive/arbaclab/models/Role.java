package it.unive.arbaclab.models;

public class Role extends SimpleNameEntity implements Comparable<Role> {
	public Role(String name) {
		super (name);
	}

	@Override
	public int compareTo(Role that) {
		if (this == that) return 0;
		else if (that == null) return -1;
		else return this.getName().compareTo(that.getName());
	}
}
