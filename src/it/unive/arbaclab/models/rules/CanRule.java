package it.unive.arbaclab.models.rules;

import java.util.Set;

import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.User;

public abstract class CanRule {
	private Role has;
	private Role target;
		
	public CanRule(Role has, Role target) {
		this.has = has;
		this.target = target;
	}
	
	public Role getHas() {
		return has;
	}
	public void setHas(Role has) {
		this.has = has;
	}
	public Role getTarget() {
		return target;
	}
	public void setTarget(Role target) {
		this.target = target;
	}
	
	public abstract boolean satisfy(User user, Set<Role> roles);
	public abstract void apply(User user, Set<Role> roles);
}
