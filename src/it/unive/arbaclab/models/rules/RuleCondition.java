package it.unive.arbaclab.models.rules;

import it.unive.arbaclab.models.Role;

public class RuleCondition {
	private Role role;
	private boolean negative = false;
	private boolean alwaysTrue = false;
	
	public RuleCondition(Role role, boolean negative, boolean alwaysTrue){
		this.role = role;
		this.negative = negative;
		this.alwaysTrue = alwaysTrue;
	}
	
	public Role getRole() {
		return role;
	}
	public boolean isNegative() {
		return this.negative;
	}

	public boolean isAlwaysTrue() {
		return this.alwaysTrue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (alwaysTrue ? 1231 : 1237);
		result = prime * result + (negative ? 1231 : 1237);
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleCondition other = (RuleCondition) obj;
		if (alwaysTrue != other.alwaysTrue)
			return false;
		if (negative != other.negative)
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		if (this.alwaysTrue) {
			sb.append(Boolean.TRUE.toString().toUpperCase());
		} else {
			if (this.negative) {
				sb.append("-");
			}
			
			sb.append(this.role.getName());
		}
		
		return sb.toString();
	}
}
