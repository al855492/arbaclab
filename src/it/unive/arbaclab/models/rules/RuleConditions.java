package it.unive.arbaclab.models.rules;

import java.util.LinkedHashSet;

/*
 * RuleConditions is nothing else then a LinkedHashSet with a cool toString() method
 */
public class RuleConditions extends LinkedHashSet<RuleCondition> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6951727644273523511L;


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		int i = 0;
		for (RuleCondition rule : this) {
			sb.append(rule.toString());
			
			if (i < this.size() - 1) {
				sb.append("&");
			}
			
			i++;
		}
		
		return sb.toString();
	}
}
