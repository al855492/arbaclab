package it.unive.arbaclab.models.rules;

import java.util.Set;

import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.User;

public class CanAssignRule extends CanRule {
	private RuleConditions conditions;

	public CanAssignRule(Role has, RuleConditions conditions, Role assign) {
		super (has, assign);
		this.conditions = conditions;
	}
	
	public RuleConditions getConditions() {
		return conditions;
	}
	public void setConditions(RuleConditions conditions) {
		this.conditions = conditions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getTarget() == null) ? 0 : this.getTarget().hashCode());
		result = prime * result + ((this.getConditions() == null) ? 0 : this.getConditions().hashCode());
		result = prime * result + ((this.getHas() == null) ? 0 : this.getHas().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CanAssignRule other = (CanAssignRule) obj;
		if (this.getTarget() == null) {
			if (other.getTarget() != null)
				return false;
		} else if (!this.getTarget().equals(other.getTarget()))
			return false;
		if (this.conditions == null) {
			if (other.conditions != null)
				return false;
		} else if (!this.conditions.equals(other.conditions))
			return false;
		if (this.getHas() == null) {
			if (other.getHas() != null)
				return false;
		} else if (!this.getHas().equals(other.getHas()))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<");
		
		sb.append(this.getHas() + ",");
		sb.append(this.conditions.toString());
		sb.append("," + this.getTarget());
		
		sb.append(">");
		
		return sb.toString();
	}

	@Override
	public boolean satisfy(User user, Set<Role> roles) {
		boolean satisfied = true;
		
		// Given a user and its roles checks if this one satisfy all the conditions
		// required for receive the new role
		
		for (RuleCondition condition : this.conditions) {
			if (!condition.isAlwaysTrue()) {
				// Checking if user contains this role
				boolean cond = roles.contains(condition.getRole());
				
				// Inverse logic
				if (condition.isNegative()) {
					cond = !cond;
				}
				
				satisfied = cond;
			}
			
			// Stop. All the conditions must be satisfied
			if (!satisfied)
				break;
		}
		
		return satisfied;
	}

	@Override
	public void apply(User user, Set<Role> roles) {
		// Adding the new role to the user
		roles.add(this.getTarget());
	}

}
