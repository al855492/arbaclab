package it.unive.arbaclab.models.rules;

import java.util.Set;

import it.unive.arbaclab.models.Role;
import it.unive.arbaclab.models.User;

public class CanRevokeRule extends CanRule {
	
	public CanRevokeRule(Role has, Role target) {
		super(has, target);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getHas() == null) ? 0 : this.getHas().hashCode());
		result = prime * result + ((this.getTarget() == null) ? 0 : this.getTarget().hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CanRevokeRule other = (CanRevokeRule) obj;
		if (this.getHas() == null) {
			if (other.getHas() != null)
				return false;
		} else if (!this.getHas().equals(other.getHas()))
			return false;
		if (this.getTarget() == null) {
			if (other.getTarget() != null)
				return false;
		} else if (!this.getTarget().equals(other.getTarget()))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "<" + this.getHas()+ "," + this.getTarget() + ">";
	}
	
	@Override
	public boolean satisfy(User user, Set<Role> roles) {
		// Simply check if the user contains the rule to be revoked
		return roles.contains(this.getTarget());
	}

	@Override
	public void apply(User user, Set<Role> roles) {
		// Revoke the target role
		roles.remove(this.getTarget());
	}	
}
