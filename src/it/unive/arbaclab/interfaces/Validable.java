package it.unive.arbaclab.interfaces;

public interface Validable {
	public boolean isValid();
}
