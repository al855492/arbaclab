package it.unive.arbaclab.interfaces;

public interface Identifiable {
	public String identifier();
}
